###Cookie Testing

###Live Server HTTPS
1. [How to](https://webisora.com/blog/how-to-enable-https-on-live-server-visual-studio-code/)
1. [Create self-signed cert](https://gist.github.com/exAspArk/f738f0771e2675e7f4c3b5d11403efd8)
1. Add cert to Keychain and configure it to be always trusted.
1. Deploy self-signed certs.
1. Update `.vscode/settings.json` file to reflect deployed location.

####Resources

https://stackoverflow.com/questions/43002444/make-axios-send-cookies-in-its-requests-automatically

https://stackoverflow.com/questions/34558264/fetch-api-with-cookie

```
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:5500');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
```

Click to video YouTube video

[![](http://img.youtube.com/vi/nfNrfi7HmLs/0.jpg)](http://www.youtube.com/watch?v=nfNrfi7HmLs "YouTube Video")